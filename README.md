# Slinks-Adventure
This is the first game I am making. I plan on having continous updates and support Windows, macOS, Linux and the Nintendo Switch.

# What is the game about?
You are Slink. You're world where other slink-like creatures are has been overrun by a bad guy (need to think of a name) and it is your job to bring the world back from the darkness

(I'll write a better story soon...)

# Downloads
Windows x64: https://bit.ly/2OLPz3v

macOS: https://bit.ly/2w42HKA

Linux: https://bit.ly/38kwnkS

Switch: https://bit.ly/2Hh3YR4

# TODO:
This is a todo list for what I need to complete before Slink's Adventure can be considered "Alpha"
- [ ] 3 Levels each with 4 stages
- [ ] Make custom title screen
- [ ] Get Title screen to work properly on Switch port.
- [ ] Make custom Icon game icon
- [ ] Make basic sound effects
